Bioinformatic analysis of microbial diversity
=============================================


**Bacteria OperationalTaxonomic Units (OTUs)**

Open a terminal and move to the folder::

      cd /opt/icme11-embo/otu/bacteria


Move fastq files ::

     cp /opt/data/bacteria/*fastq .


Lunch the script and answer to the questions (use "sample_list.txt" file)::

     ./Illumina.sh


**Fungi OperationalTaxonomic Units (OTUs)**

Open a terminal and move to the folder::

      cd /opt/icme11-embo/otu/fungi


Move fastq files ::

     cp /opt/data/fungi/*fastq .


Lunch the script and answer to the questions (use "sample_list.txt" file)::

     ./Illumina_fungi.sh


**Fungi and Bacteria Amplicon Sequence variants (ASVs)**

Move bacteria sequence input to R1-16S and R2-16S folders::

     cp /opt/icme11-embo/data/bacteria/*.R1.fastq /opt/icme11-embo/asv/R1-16S
     cp /opt/icme11-embo/data/bacteria/*.R2.fastq /opt/icme11-embo/asv/R2-16S

Move fungi sequence input to R1-ITS and R2-ITS folders::

     cp /opt/icme11-embo/data/fungi/*.R1.fastq /opt/icme11-embo/asv/R1-ITS
     cp /opt/icme11-embo/data/fungi/*.R2.fastq /opt/icme11-embo/asv/R2-ITS


Open Rstudio::
     rstudio &

Run the scripts::
      asv.filtering.R
      
      
Move the filtered reads::

     cp /opt/icme11-embo/asv/R1-16S/filtered1/*fastq /opt/icme11-embo/asv/R1/filtered1
     cp /opt/icme11-embo/asv/R2-16S/filtered1/*fastq /opt/icme11-embo/asv/R2/filtered1

     cp /opt/icme11-embo/asv/R1-ITS/filtered1/*fastq /opt/icme11-embo/asv/R1/filtered1
     cp /opt/icme11-embo/asv/R2-ITS/filtered1/*fastq /opt/icme11-embo/asv/R2/filtered1
	  
Run the the scripts::

     asv.inference.R


**Collembola MOTUs**

Open a terminal and move to the folder::

      cd /opt/icme11-embo/otu/collembola
      
Move files::
      
      mv /opt/data/bacteria/*fastq .
      
Lunch the script and answer to the questions (use "sample_list.txt" file)::

     ./Illumina_collembola.sh
     
     
