Operational Taxonomic Units (OTUs)
==================================

**Define OTUs and their abundance**

Move to data folder::

      cd $HOME/MEM(MA)_practical/amplicons


Move fastq files ::

     cp *fastq /Microbiology/amplicons/


Lunch the script and answer to the questions (use name_list file)::

     ./Illumina.sh


